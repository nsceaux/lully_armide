\clef "haute-contre" \ru#53 { R2.\allowPageTurn } |
r4 r si'4 |
si'2 si'4 |
la'4 la'4 re''4 |
do''4 si'4.\tr do''8 |
do''2 r4 |
R2.*3 |
r4 r sold'4 |
la'2 la'4 |
la'4 la'4 la'4 |
la'4 sold'4. la'8 |
la'2 la'4 |
la'2 sol'4 |
mi'4. sol'8 la'4 |
si'8 do''8 re''4. re''8 |
si'2\tr r4 |
R2.*6 |
r4 r do''4 |
do''2 do''4 |
la'4. si'8 fad'4 |
sol'8. la'16 la'4.\tr sol'8 |
sol'2 si'4 |
si'2 si'4 |
la'4 la'4 re''4 |
do''4 si'4.\tr do''8 |
do''2 r4 |
R2.*3 |
r4 r la'4 |
sib'2 sib'4 |
\ficta sib'?4 do''4 do''4 |
re''4 re''4. re''8 |
si'2 r4 |
R2.*3 |
r4 r si'4 |
si'2 si'4 |
la'4 la'4 re''4 |
do''4 si'4.\tr do''8 |
do''2. |
