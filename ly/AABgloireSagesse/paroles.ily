%% La Gloire
\tag #'(voix0 basse) {
  Tout doit ce -- der dans l’u -- ni -- vers
  à l’au -- gus -- te he -- ros que j’ai -- me.
  L’ef -- fort des en -- ne -- mis, les gla -- ces des hy -- vers,
  les ro -- chers, les fleu -- ves, les mers,
  rien n’ar -- res -- te l’ar -- deur de sa va -- leur ex -- tré -- me.
  Tout doit ce -- der dans l’u -- ni -- vers
  à l’au -- gus -- te He -- ros que j’ai -- me.
}
%% La Sagesse
\tag #'(voix1 basse) {
  Tout doit ce -- der dans l’u -- ni -- vers
  à l’au -- gus -- te he -- ros que j’ai -- me.
  Il sçait l’art de te -- nir tous les mons -- tres aux fers :
  il est maistre ab -- so -- lu de cent peu -- ples di -- vers,
  et plus maistre en -- cor de luy- mes -- me.
}
%% La Gloire & la Sagesse
\tag #'(voix0 voix1 basse) {
  Tout doit ce -- der dans l’u -- ni -- vers
  à l’au -- gus -- te He -- ros que j’ai -- me.
}
\tag #'(voix1 basse) {
  Chan -- tons, chan -- tons la dou -- ceur de ses loix.
}
\tag #'(voix0 basse) {
  Chan -- tons, chan -- tons ses glo -- ri -- eux ex -- ploits.
}

%% Chœurs
\tag #'(vdessus vhaute-contre basse) {
  Chan -- tons, chan -- tons la dou -- ceur de ses loix.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Chan -- tons, chan -- tons ses glo -- ri -- eux ex -- ploits.
}
\tag #'(vdessus vhaute-contre basse) {
  Chan -- tons, chan -- tons la dou -- ceur de ses loix.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Chan -- tons, chan -- tons ses glo -- ri -- eux ex -- ploits.
  Chan -- tons, chan -- tons ses glo -- ri -- eux ex -- ploits.
}
\tag #'(vdessus basse) {
  Chan -- tons la dou -- ceur de ses loix.
}
\tag #'(vhaute-contre) {
  Chan -- tons, __
}
\tag #'(vdessus vhaute-contre basse) {
  Chan -- tons, chan -- tons la dou -- ceur de ses loix.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Chan -- tons, chan -- tons ses glo -- ri -- eux ex -- ploits.
  Chan -- tons, chan -- tons ses glo -- ri -- eux ex -- ploits.
}

\tag #'(vdessus vhaute-contre) {
  Chan -- tons, chan -- tons la dou -- ceur de ses loix.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Chan -- tons, chan -- tons ses glo -- ri -- eux ex -- ploits.
}
