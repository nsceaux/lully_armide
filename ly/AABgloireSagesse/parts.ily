\piecePartSpecs
#`((dessus #:score "score-dessus")
   (haute-contre #:score-template "score-voix")
   (taille #:score-template "score-voix")
   (quinte #:score-template "score-voix")
   (basse #:score-template "score-basse-continue-voix"
          #:tag-notes tous))
