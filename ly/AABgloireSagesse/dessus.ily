\clef "dessus" \ru#53 { R2.\allowPageTurn } |
r4 r re''4^\markup Violons |
sol''2 sol''4 |
do''4 do''4 re''4 |
mi''4 re''4.\tr do''8 |
do''2 r4 |
R2.*3 |
r4 r si'4 |
mi''2 mi''4 |
do''4 do''4 do''4 |
si'4 si'4.\tr la'8 |
la'2 do''4 |
re''2 re''4 |
mi''4. mi''8 fad''4 |
sol''4 fad''4. sol''8 |
sol''2 r4 |
R2.*6 |
r4 r mi''4 |
mi''2 la''4 |
fad''4. re''8\tr do''4 |
si'4 la'4.\tr re''8 |
si'2\tr re''4 |
sol''2 sol''4 |
do''4 do''4 re''4 |
mi''4 re''4.\tr do''8 |
do''2 \twoVoices #'(un deux tous) <<
  { sol''4 |
    do'''2 do'''4 |
    la''4\tr la''4 sib''4 |
    do'''4 do'''4 sib''4 |
    la''2 }
  { mi''4 |
    la''2 la''4 |
    fad''4 fad''4 sol''4 |
    la''4 la''4 sol''4 |
    fad''2 }
>> re''4 |
sib''2 sib''4 |
sol''4. sol''8 la''4 |
sib''4 la''4. sol''8 |
sol''2 r4 |
R2.*3 |
r4 r re'' |
sol''2 sol''4 |
do'' do'' re'' |
mi'' re''4.\tr do''8 |
do''2. |
