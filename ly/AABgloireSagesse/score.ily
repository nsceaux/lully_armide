\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff <<
        { s2.*54 \noHaraKiri }
        \global \keepWithTag #'tous \includeNotes "dessus"
      >>
      \new Staff <<
        { s2.*54 \noHaraKiri }
        \global \includeNotes "haute-contre"
      >>
      \new Staff <<
        { s2.*54 \noHaraKiri }
        \global \includeNotes "taille"
      >>
      \new Staff <<
        { s2.*54 \noHaraKiri }
        \global \includeNotes "quinte"
      >>
      \new Staff <<
        { s2.*54 \noHaraKiri }
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        { s2.*50 \noHaraKiri }
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s2.*50 \noHaraKiri }
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s2.*50 \noHaraKiri }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      %% Basses
      \new Staff \withLyrics <<
        { s2.*50 \noHaraKiri }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix0 \includeNotes "voix"
    >> \keepWithTag #'voix0 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff <<
      { s2.*22\break s2.*14\break s2.*13 s2 \bar "" \break }
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}
