<<
  %% La Gloire
  \tag #'(voix0 basse) {
    \gloireMark
    do''4 sol'4. do''8 |
    la'4 do''4 re''8 mi''8 |
    re''2 re''8 mi''8 |
    fa''2 fa''8 sol''8 |
    mi''8 fa''8 mi''4( re''4\tr) |
    do''2 r8 mi''8 |
    re''4.\tr re''8 re''8. sol''16 |
    do''2 mi''4 |
    la'4. la'8 la'8 si'8 |
    sold'2 si'8 mi''8 |
    dod''2 r8 la'8 |
    fad'4 fad'4 r8 re''8 |
    si'2\tr sol'8 sol'8 |
    do''4 do''8 re''8 mi''8 mi''8 |
    mi''8 mi''8 fad''8 sol''8 la'4\tr |
    sol'2 r4 |
    do''4 sol'4. do''8 |
    la'4 do''4 re''8 mi''8 |
    re''2\tr re''8 mi''8 |
    fa''2 fa''8 sol''8 |
    mi''8 fa''8 mi''4( re''4\tr) |
    do''2. |
    <<
      \tag #'basse { s2.*14 \gloireMark }
      \tag #'voix0 { R2.*14 <>^\markup\character La Gloire }
    >>
    mi''4 mi''4. sol''8 |
    do''4 do''4 re''8 mi''8 |
    re''2\tr re''8 mi''8 |
    fa''2 fa''8. sol''16 |
    mi''8 fa''8 mi''4( re''4\tr) |
    do''2
    <<
      \tag #'basse { s4 s2.*3 s2 \gloireMark }
      \tag #'voix0 { r4 R2.*3 r4 r <>^\markup\character La Gloire }
    >> re''4 |
    si'2 si'4 |
    mi''4. mi''8 fad''4 |
    sol''4 fad''4. sol''8 |
    sol''2
    \tag #'voix0 { r4 | R2.*51 }
  }
  %% La Sagesse
  \tag #'(voix1 basse) {
    \clef "vbas-dessus"
    <<
      \tag #'basse { s2.*22 \sagesseMark }
      \tag #'voix1 { R2.*22 <>^\markup\character La Sagesse }
    >>
    do''4 sol'4. do''8 |
    la'4 do''4 re''8 mi''8 |
    re''2\tr re''8 mi''8 |
    fa''2 fa''8 sol''8 |
    mi''8 fa''8 mi''4( re''4\tr) |
    do''2 mi''8 sol''8 |
    do''4 do''8 sol'8 la'4 |
    la'8 do''8 fa'4 fa'8 sol'8 |
    mi'2\tr sol'8 sol'8 |
    sol'4 la'8 si'8 do''4 |
    do''8 re''8 mi''4 fad''8 sol''8 |
    fad''2 re''8 sol''8 |
    do''8 si'8 la'4 la'8 si'8 |
    sol'4 sol'4 r4 |
    <<
      \tag #'basse { s2.*5 s2 \sagesseMark }
      \tag #'voix1 {
        <>^\markup\character La Sagesse
        do''4 sol'4. sib'8 |
        la'4 la'4 si'8 do''8 |
        si'2 si'8 dod''8 |
        re''2 la'8 si'8 |
        do''8 re''8 do''4( si'4\tr) |
        do''2
      }
    >> mi''4^\viste |
    re''2 re''4 |
    do''4 do''4 re''4 si'4 la'4 sol'4 |
    fad'2
    \tag #'voix1 { r4 R2.*55 }
  }
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s2.*49 s2 \ffclef "vdessus" <>^\markup\character Chœur }
      \tag #'vdessus {
        \clef "vdessus" R2.*49 |
        r4 r <>^\markup\character "Suite de la Sagesse"
      }
    >>
    <<
      { \tag #'vdessus \voiceOne re''4 | \noBreak
        sol''2 sol''4 |
        do''4 do''4 re''4 |
        mi''4 mi''4 fa''4 |
        re''2\tr \oneVoice }
      \tag #'vdessus \new Voice \with { autoBeaming = ##f } {
        \voiceTwo si'4 |
        mi''2 mi''4 |
        la'4 la'4 si'4 |
        do''4 do''4 re''4 |
        si'2\tr
      }
    >>
    <<
      \tag #'vdessus {
        <>^\markup\character "Suite de la Gloire" re''4 |
        \noBreak sol''2 sol''4 |
        do''4 do''4 re''4 |
        mi''4 re''4.\tr do''8 |
        do''2
      }
      \tag #'basse { r4 R2.*3 r4 r }
    >>
    <<
      { \tag #'vdessus \voiceOne mi''4 |
        re''2 mi''4 |
        do''4 do''4 do''4 |
        fa''4 fa''4 re''4 |
        si'2 \oneVoice }
      \tag #'vdessus \new Voice \with { autoBeaming = ##f } {
        \voiceTwo do''4 |
        si'2 do''4 |
        la'4 la'4 la'4 |
        la'4 la'4 si'4 |
        sold'2
      }
    >>
    <<
      \tag #'basse { r4 R2.*7 r4 r }
      \tag #'vdessus {
        si'4 |
        mi''2 mi''4 |
        do''4 do''4 do''4 |
        si'4 si'4. la'8 |
        la'2 do''4 |
        re''2 re''4 |
        mi''4. mi''8 fad''4 |
        sol''4 fad''4. sol''8 |
        sol''2
      }
    >>
    <<
      { \tag #'vdessus \voiceOne re''4 |
        re''4 re''4 mi''4 |
        fa''4 fa''4 mi''4 |
        re''2\tr sol''4 |
        mi''2 fa''4 |
        fa''4 fa''4 mi''4 |
        re''4\tr re''4 sol''4 |
        mi''2 \oneVoice }
      \tag #'vdessus \new Voice \with { autoBeaming = ##f } {
        \voiceTwo si'4 |
        si'4 si'4 do''4 |
        re''4 re''4 do''4 |
        si'2\tr si'4 |
        do''2 la'4 |
        si'4 si'4 do''4 |
        do''4 do''4 si'4 |
        do''2
      }
    >>
    <<
      \tag #'vdessus {
        mi''4 |
        mi''2 la''4 |
        fad''4. re''8 do''4 |
        si'4 la'4.\tr re''8 |
        si'2\tr re''4 |
        sol''2 sol''4 |
        do''4 do''4 re''4 |
        mi''4 re''4.\tr do''8 |
        do''2 r4 |
        R2.*7 |
        r4 r
      }
      \tag #'basse { r4 R2.*15 r4 r }
    >>
    <<
      { \tag #'vdessus \voiceOne re''4 |
        sol''2 sol''4 |
        do''4 do''4 re''4 |
        mi''4 mi''4 fa''4 |
        re''2\tr \oneVoice }
      \tag #'vdessus \new Voice \with { autoBeaming = ##f } {
        \voiceTwo si'4 |
        mi''2 mi''4 |
        la'4 la'4 si'4 |
        do''4 do''4 re''4 |
        si'2\tr
      }
    >>
    <<
      \tag #'basse { r4 R2.*4 }
      \tag #'vdessus {
        re''4 |
        sol''2 sol''4 |
        do''4 do''4 re''4 |
        mi''4 re''4.\tr do''8 |
        do''2. |
      }
    >>
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R2.*49 |
    r4 r sol'4 |
    mi'2 mi'4 |
    fa'4 fa'4 re'4 |
    la'4 la'4 fa'4 |
    sol'2 si'4 |
    si'2 si'4 |
    la'4 la'4 sol'4 |
    sol'4 sol'4. sol'8 |
    mi'2\tr do'4 |
    sol'2 mi'4 |
    fa'4 fa'4 fa'4 |
    re'4 re'4 re'4 |
    mi'2 sold'4 |
    la'2 la'4 |
    la'4 la'4 la'4 |
    la'4 sold'4. la'8 |
    la'2 la'4 |
    la'2 sol'4 |
    sol'4. sol'8 la'4 |
    si'4 la'4.\tr sol'8 |
    sol'2 sol'4 |
    sol'2. ~|
    sol'2. ~|
    sol'2 mi'4 |
    la'2 fa'4 |
    sol'4 sol'4 la'4 |
    fa'4 sol'4 sol'4 |
    do'2 sol'4 |
    la'2 la'4 |
    la'4. si'8 fad'4 |
    sol'4 fad'4. sol'8 |
    sol'2 si'4 |
    si'2 si'4 |
    la'4 la'4 sol'4 |
    sol'4 sol'4. sol'8 |
    mi'2\tr r4 |
    R2.*7 |
    r4 r sol'4 |
    mi'2 mi'4 |
    fa'4 fa'4 re'4 |
    la'4 la'4 fa'4 |
    sol'2  si'4 |
    si'2 si'4 |
    la'4 la'4 sol'4 |
    sol'4 sol'4. sol'8 |
    mi'2. |
  }
  \tag #'vtaille {
    \clef "vtaille" R2.*49 |
    R2.*4
    r4 r re'4 |
    mi'2 mi'4 |
    mi'4 fa'4 re'4 |
    do'4 si4. do'8 |
    do'2 r4 |
    R2.*3 |
    r4 r si4 |
    do'2 do'4 |
    do'4 fa'4 fa'4 |
    fa'4 mi'4 si4 |
    do'2 mi'4 |
    re'2 re'4 |
    do'4. do'8 do'4 |
    re'4 re'4. re'8 |
    si2 r4 |
    R2.*6 |
    r4 r do'4 |
    do'2 do'4 |
    re'4. sol'8 do'4 |
    re'4 re'4. re'8 |
    re'2 re'4 |
    mi'2 mi'4 |
    mi'4 fa'4 re'4 |
    do'4 si4.\tr do'8 |
    do'2 r4 |
    R2.*11 |
    r4 r re'4 |
    mi'2 mi'4 |
    mi'4 fa'4 re'4 |
    do'4 si4. do'8 |
    do'2. |
  }
  \tag #'vbasse {
    \clef "vbasse" R2.*53 |
    r4 r sol4 |
    mi2 mi4 |
    la4 la4 si4 |
    do'4 sol4 sol,4 |
    do2 r4 |
    R2.*3 |
    r4 r mi4 |
    do2 do4 |
    fa4 fa4 fa4 |
    re4 mi4 mi4 |
    la,2 la4 |
    si2 si4 |
    do'4. si8 la4 |
    sol4 re'4 re4 |
    sol2 r4 |
    R2.*6 |
    r4 r do'4 |
    la2 la4 |
    re'4. si8 la4 |
    sol4 re4 re4 |
    sol2 sol4 |
    mi2 mi4 |
    la4 la4 si4 |
    do'4 sol4 sol,4 |
    do2 r4 |
    R2.*11 |
    r4 r sol4 |
    mi2 mi4 |
    la4 la4 si4 |
    do'4 sol4 sol,4 |
    do2. |
  }
>>
