\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{\markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPersDidas La Gloire & la Sagesse ensemble
    \livretVerse#6 { D’une esgale tendresse, }
    \livretVerse#8 { Nous aimons le mesme Vainqueur. }
    \livretPers La Sagesse
    \livretVerse#12 { Fiere Gloire, c’est vous, }
    \livretPers La Gloire
    \livretVerse#12 { \transparent { Fiere Gloire, c’est vous, } C’est vous, douce Sagesse, }
    \livretPers La Gloire & la Sagesse
    \livretVerse#12 { C’est vous, qui partagez avec moy son grand cœur. }
    \livretPers La Gloire
    \livretVerse#12 { Je l’emportois sur vous tant qu’a duré la guerre ; }
    \livretVerse#10 { Mais dans la paix vous l’emportez sur moy. }
    \livretVerse#12 { Vous reglez en secret avec ce sage Roy }
    \livretVerse#8 { Le destin de toute la terre. }
    \livretPers La Sagesse
    \livretVerse#12 { La Victoire a suivy ce Heros en tous lieux ; }
    \livretVerse#10 { Mais pour montrer son amour pour la Gloire, }
    \livretVerse#6 { Il se sert encor mieux }
    \livretVerse#8 { De la paix que de la victoire. }
  }
  \column {
    \null
    \livretVerse#12 { Au milieu du repos qu’il asseure aux Humains, }
    \livretVerse#10 { Il fait tomber sous ses puissantes mains }
    \livretVerse#12 { Un Monstre qu’on a creû si longtemps invincible : }
    \livretVerse#12 { On voit dans ses travaux combien il est sensible }
    \livretVerse#8 { Pour vostre immortelle Beauté. }
    \livretVerse#12 { Il previent vos desirs, il passe vostre attente, }
    \livretVerse#12 { L’amour dont il vous aime incessemment s’augmente, }
    \livretVerse#8 { Et n’a jamais tant esclaté. }
    \livretVerse#8 { Qu’un vain desir de preference, }
    \livretVerse#8 { N’altere point l’intelligence }
    \livretVerse#10 { Que ce Heros entre nous veut former : }
    \livretVerse#12 { Disputons seulement à qui sçait mieux l’aimer. }
    \livretDidasP\justify { La Gloire repete ce dernier Vers avec la Sagesse. }
    \livretPersDidas La Gloire & la Sagesse ensemble
    \livretVerse#6 { Dés qu’on le voit paraistre, }
    \livretVerse#8 { De quel cœur n’est-il point le Maistre ? }
    \livretVerse#8 { Qu’il est doux de suivre ses pas ! }
    \livretVerse#5 { Peut-on le connaistre, }
    \livretVerse#5 { Et ne l’aimer pas ? }
  }
}#}))
