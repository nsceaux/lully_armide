\clef "haute-contre" R1*43 R1.*3 R1*3 R1. R1*9 R1. R1*2 R2.*30 |
la'4 la'4 la'4 |
la'2 la'4 |
sol'2 sol'8 sol'8 |
sol'4 sol'4 sol'4 |
la'2 re'4 |
mi'2 mi'8 mi'8 |
fa'2 sol'4 |
re''4 si'4 do''4 |
si'2\tr r4 |
R2.*3 |
r4 r do''4 |
do''4 do''4 la'4 |
fad'2 sol'4 |
sol'8 la'8 si'4. si'8 |
sold'2 r4 |
R2.*3 |
r4 la'4 la'8 la'8 |
la'2 la'4 |
sol'2 sol'8 sol'8 |
sol'4 sol'4 sol'4 |
la'2 la'4 |
fad'2 sol'8 sol'8 |
lad'2 si'4 |
si'4 si'4 si'4 |
si'2 r4 |
R2.*3 |
r4 r sol'8 sol'8 |
sol'2 sol'4 |
la'4 fad'4 sol'4 |
fad'2 r4 |
R2.*3 |
r4 r si'4 |
si'4 si'4 si'4 |
do''2 do''4 |
do''4 si'4.\tr do''8 |
do''2 do''8 do''8 |
sold'2 la'4 |
si'4 mi'4 mi'4 |
mi'2 sold'4 |
do''4 do''4 la'4 |
la'2 la'4 |
la'4 sold'4. la'8 |
la'2 r4 |
R2.*3 |
r4 r la'8 la'8 |
sold'2 la'4 |
si'4 mi'4 mi'4 |
mi'2 r4 |
R2.*3 |
r4 r mi'4 |
fa'4 fa'4 fa'4 |
mi'2 la'4 |
la'4 sold'4. la'8 |
la'2. |
