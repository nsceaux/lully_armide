\clef "dessus"
R1*43 R1.*3 R1*3 R1. R1*9 R1. R1*2 R2.*30 |
mi''4^\markup Violons mi''4 do''4 |
fa''2 fa''4 |
mi''2\tr do''8 do''8 |
si'4 si'4 dod''4 |
re''2 si'4 |
do''2 do''8 mi''8 |
la'2 do''4 |
fa''4 fa''4 mi''4 |
re''2\tr r4 |
R2.*3 |
r4 r sol''4 |
sol''4 fad''4 fad''4 |
red''2 si'4 |
mi''4 red''4. mi''8 |
mi''2 r4 |
R2.*3 |
r4 do''4 do''8 do''8 |
la'2 re''4 |
si'2\tr si'8 si'8 |
mi''4 mi''4 mi''4 |
dod''2 fad''4 |
red''2 sol''8 sol''8 |
red''2 mi''4 |
fad''4 fad''4 sol''4 |
fad''2 \twoVoices #'(un deux tous) <<
  { fad''4 |
    sol''4 la''4 si''4 |
    la''4\tr sol''4 fad''4 |
    sol''4 fad''4. mi''8 |
    mi''2 }
  { red''4 |
    mi''4 fad''4 sol''4 |
    fad''4\tr mi''4 red''4 |
    mi''4 red''4. mi''8 |
    mi''2 }
>> si'8 si'8 |
mi''2 re''4 |
do''4 do''4 si'4 |
la'2\tr r4 |
R2.*3 |
r4 r re''4 |
sol''4 sol''4 sol''4 |
mi''2\tr mi''4 |
fa''4 re''4.\tr do''8 |
do''2 mi''8 mi''8 |
re''2\tr do''4 |
si'4 si'4 do''4 |
sold'2 si'4 |
mi''4 mi''4 mi''4 |
do''2 do''4 |
si'4 si'4. la'8 |
la'2 \twoVoices #'(un deux tous) <<
  { mi''4 |
    fa''4 sol''4 la''4 |
    sol''4\tr fa''4 mi''4 |
    fa''4 mi''4.\tr re''8 |
    re''2 }
  { dod''4 |
    re''4 mi''4 fa''4 |
    mi''4\tr re''4 dod''4 |
    re''4 dod''4. re''8 |
    re''2 }
>> fa''8 mi''8 |
re''2\tr do''4 |
si'4 si'4 do''4 |
sold'2 r4 |
R2.*3 |
r4 r do''4 |
la'4 la'4 si'4 |
sold'2 do''4 |
si'4 si'4.\tr la'8 |
la'2. |
