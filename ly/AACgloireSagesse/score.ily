\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \withLyrics <<
      \characterName \markup "La Gloire"
      \global \keepWithTag #'gloire \includeNotes "voix"
    >> \keepWithTag #'gloire \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \characterName \markup "La Sagesse"
      \global \keepWithTag #'sagesse \includeNotes "voix"
    >> \keepWithTag #'sagesse \includeLyrics "paroles"
    \new Staff <<
      \instrumentName "Basse Continue"
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
