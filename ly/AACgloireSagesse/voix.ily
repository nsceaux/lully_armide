<<
  %% La Gloire
  \tag #'(gloire basse) {
    \clef "vbas-dessus"
    \tag #'basse \gloireMark
    r2^\markup\orig-version\character "La Gloire" mi''4. mi''8 |
    re''2 re''4 re''4 |
    do''2 do''4 mi''8 mi''8 |
    fad''4 sol''4 sol''4 sol''8 fad''8 |
    sol''2
    <<
      \tag #'basse { s2 s1 s2 \gloireMark }
      \tag #'gloire { r2 | R1 | r2 }
    >> r4 do''4 |
    fa''2 la'4 la'8 re''8 |
    si'2\tr si'4 re''4 |
    mi''4 mi''4 do''4 do''4 |
    fa''2 re''4 re''4 |
    mi''2 re''4.\tr do''8 |
    do''4 sol''4 mi''4 mi''4 |
    fa''4 fa''4 sol''4. sol''8 |
    do''2 do''4 re''4 |
    mi''2 re''4.\tr do''8 |
    do''4 r8 sol'8 sol'4 sol'4 |
    do''4 re''4 mi''2 |
    mi''4 mi''8 mi''8 re''4.\tr do''8 |
    sol''2 mi''4 do''4 |
    la'4.\tr la'8 fad'4. re''8 |
    re''4 mi''4 la'4.\tr sol'8 |
    sol'2 r4 re''8 mi''8 |
    fa''2 la'4 si'4 |
    do''4. do''8 do''4 sib'4 |
    sib'4. do''8 la'2\tr |
    fa''4 re''4 sol''4. sol''8 |
    fa''4 fa''8 mi''8 re''2\tr |
    do''2
    <<
      \tag #'basse {
        s2 s1*14 s1.*3 s1*3 s1. s1*9 s1. s1*2 s2.*13 s2
        \gloireMark
      }
      \tag #'gloire {
        r2 | R1*14 | R1.*3 | R1*3 | R1. | R1*9 | R1. | R1*2 | R2.*13 | r4 r
        <>^\markup\character La Gloire
      }
    >> mi''8 fa''8 | \noBreak
    sol''2 sol''8 sol''8 |
    fa''4 re''4 si'4 |
    do''4 si'4.\tr la'8 |
    la'2. |
    mi''4 mi''4 mi''4 |
    do''2 do''4 |
    fa''4 fa''4 fa''8 fa''8 |
    re''4\tr re''8 re''8 mi''8 fa''8 |
    sol''4 sol''4 mi''8 mi''8 |
    re''2\tr re''4 |
    do''4.\tr si'8 do''4 |
    si'2\tr si'4 |
    do''4 re''4 mi''4 |
    re''4\tr do''4 si'4 |
    do''4 si'4.\tr la'8 |
    la'2. |
  }
  %% La Sagesse
  \tag #'(sagesse basse) {
    <<
      \tag #'basse { s1*4 s2 \sagesseMark }
      \tag #'sagesse {
        \clef "vbas-dessus"
        r2^\markup\orig-version\character "La Sagesse" do''4. do''8 |
        do''2 si'4 si'4 |
        mi'2 mi'4 do''8 do''8 |
        do''4 si'4 la'4\tr la'8 re''8 |
        si'2
      }
    >> re''4. re''8 |
    sol''2 sol''4 do''4 |
    la'2\tr
    <<
      \tag #'basse { s2 s1*21 s2 \sagesseMark }
      \tag #'sagesse {
        r2 |
        R1 |
        r2 r4 si'4 |
        do''4 do''4 la'4\tr la'4 |
        re''2 si'4 si'4 |
        do''2 si'4.\tr do''8 |
        do''2 r4 do''4 |
        sib'4 sib'4 sib'4. do''8 |
        la'2\tr la'4 si'4 |
        do''2 si'4.\tr do''8 |
        do''4 r4 r2 |
        R1*11 |
        r2 <>^\markup\character La Sagesse
      }
    >> do''4 do''4 |
    re''2 re''4 re''4 |
    mi''2 mi''4 mi''4 |
    fa''2 fa''4 mi''4 |
    re''2\tr si'4 si'8 si'8 |
    do''4 do''8 re''8 mi''4 fad''8 sol''8 |
    fad''2 fad''4 re''8 re''8 |
    re''2 do''4 re''4 |
    si'4 si'8 si'8 do''4 do''8 do''8 |
    re''4 mi''4 re''2\tr |
    do''1 |
    r2 do''4 do''4 |
    do''2 sib'4( la'8) sib'8 |
    la'2 la'4 si'4 |
    do''2 do''4 re''4 |
    si'2\tr r4 mi''4 do'' mi'' |
    la'4 la'4 la'4. do''8 fa'4( mi'8) fa'8 |
    mi'4 sol'4 do''4 do''4 dod''4 dod''4 |
    re''4 re''8 re''8 mi''4 mi''8 mi''8 |
    fa''2 fa''4 re''4 |
    si'4 si'4 do''4 re''4 |
    mi''4 re''4 do''4 si'4 la'4 sol'4 |
    fad'2 fad'4 re''4 |
    mi''2 fad''4 sol''4 |
    la'2 la'4 si'4 |
    sol'2 si'4. si'8 |
    do''2 do''4. do''8 |
    la'2 r4 la'4 |
    re''4. re''8 si'4 si'4 |
    sold'2 sold'4 r8 mi'8 |
    la'4 la'4 la'4 si'4 |
    do''4 do''4 do''4 do''4 re''4 mi''4 |
    re''4\tr re''4 fa''4 fa''8 fa''8 |
    fa''4 mi''4 re''4.\tr mi''8 |
    do''2 r4 |
    R2. |
    do''4 do''4. la'8 |
    mi''4. mi''8 mi''8 si'8 |
    do''4 la'8 do''8 fa''8 fa''8 |
    re''4.\tr re''8 mi''8 fa''8 |
    mi''4\tr do''8 mi''8 mi''8 fa''8 |
    sol''4 do''4. do''8 |
    la'4 la'4 si'4 |
    sold'2 si'8 mi''8 |
    dod''4 re''4 mi''4 |
    fa''4 re''4 si'4 |
    do''4 si'4.\tr la'8 |
    la'2
    \tag #'sagesse {
      do''8 re''8 |
      mi''2 mi''8 mi''8 |
      la'4 si'4 sold'4 |
      la'4 sold'4. la'8 |
      la'2. |
      do''4 do''4 do''4 |
      la'2\tr la'4 |
      re''4 re''4 re''8 re''8 |
      si'4 si'8 si'8 do''8 re''8 |
      mi''4 mi''4 do''8 do''8 |
      si'2 si'4 |
      mi'4 mi'4 la'4 |
      sold'2 sold'4 |
      la'4 si'4 do''4 |
      si'4\tr la'4 sold'4 |
      la'4 sold'4. la'8 |
      la'2. |
    }
  }
>>
