% 16
s1 <4>2 {<5->} s1 <7 _+>2 \new FiguredBass { <4>4 <3> } s1 <6>
s1*2 s1 <6> s <6>
s <6->2 <5-> s2. <6>4 s1*2 s2 <6>
s2 <6>4. <6+>8 s1 <6>2 <_+> <6>2 <_+> s2. <6 4>8 <6+> s2. <6>4
% 17
s2 <6>4. <5->8 <5->1 s2 <6> <6>1 s <4 2>2 {<5->} s1
<6>2 {<5->} s1 <6>4. <6>8 s4 <6+>4 <_+>1 <7>2 {<5->2}
s <6> <6>1 s1*2 <4>2 <_-> s2. <6>4 s2 <5->
s1 <6>2 s1 <7>4 <6> s1 <_+>2 <6>1
% 18
s1*2 s2 <6+>1 <_+>2. <6>4 s2 <6+> <4>1
s <6> s1 s2 <6> <_+>1 <6>2 <6>4 <6+> s1 <6>2
s1 {<5->1} s2.*3 <6>2. s2 <6>4
<6>2 {<5->4} s2 <6 4>8 <6> <6>2. s2 <6>4 <_+>2. <_+>4 <6> <6+> s2 <_+>4
% 19
s2. s2. { <5->2. <_->2 } <_+>4 s <_+>2 s2. s2 <6>4 s2.
s2. s2 <6>8 <6> s2. s4 {<5->2} s2. <_+>
<6>4 <6+>2 {<5->2} <_+>4 <7>2. s
