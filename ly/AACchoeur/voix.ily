<<
  %% Suite de la Gloire
  \tag #'(vdessus basse) {
    \clef "vdessus"
    <<
      \tag #'basse { R2.*8 r4 r }
      \tag #'vdessus {
        mi''4 mi''4 do''4 |
        fa''2 fa''4 |
        mi''4 mi''4 do''8 do''8 |
        si'4 si'4 dod''4 |
        re''2 si'4 |
        do''4 do''4 do''8 mi''8 |
        la'2 do''4 |
        fa''4 fa''4 mi''4 |
        re''2\tr
      }
    >>
    <<
      { \tag #'vdessus \voiceOne re''4 |
        mi''4 fa''4 sol''4 |
        fa''4\tr mi''4 re''4 |
        mi''4 re''4.\tr do''8 |
        do''2 \oneVoice }
      \tag #'vdessus \new Voice \with { autoBeaming = ##f } {
        \voiceTwo si'4 |
        do''4 re''4 mi''4 |
        re''4\tr do''4 si'4 |
        do''4 si'4.\tr do''8
        do''2
      }
    >>
    <<
      \tag #'basse { r4 R2.*3 r4 r }
      \tag #'vdessus {
        sol''4 |
        sol''4 fad''4 fad''4 |
        red''4 red''4 si'4 |
        mi''4 red''4. mi''8 |
        mi''2
      }
    >>
    <<
      { \tag #'vdessus \voiceOne si'4 |
        do''4 re''4 mi''4 |
        re''4\tr do''4 si'4 |
        do''4 si'4.\tr la'8 |
        la'4 \oneVoice }
      \tag #'vdessus \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sold'4 |
        la'4 si'4 do''4 |
        si'4\tr la'4 sold'4 |
        la'4 sold'4. la'8 |
        la'4
      }
    >>
    <<
      \tag #'basse { r4 r R2.*14 r4 r }
      \tag #'vdessus {
        do''4 do''8 do''8 |
        la'2\tr re''4 |
        si'4 si'4 si'8 si'8 |
        mi''4 mi''4 mi''4 |
        dod''2 fad''4 |
        red''4 red''4 sol''8 sol''8 |
        red''2 mi''4 |
        fad''4 fad''4 sol''4 |
        fad''2 r4 |
        R2.*3 |
        r4 r si'8 si'8 |
        mi''2 re''4 |
        do''4 do''4 si'4 |
        la'2
      }
    >>
    <<
      { \tag #'vdessus \voiceOne la'4 |
        si'4 do''4 re''4 |
        do''4\tr si'4 la'4 |
        si'4 la'4.\tr sol'8 |
        sol'2 \oneVoice }
      \tag #'vdessus \new Voice \with { autoBeaming = ##f } {
        \voiceTwo fad'4 |
        sol'4 la'4 si'4 |
        la'4\tr sol'4 fad'4 |
        sol'4 fad'4. sol'8 |
        sol'2
      }
    >> 
    <<
      \tag #'basse { r4 R2.*17 r4 r }
      \tag #'vdessus {
        re''4 |
        sol''4 sol''4 sol''4 |
        mi''4 mi''4 mi''4 |
        fa''4 re''4.\tr do''8 |
        do''2 mi''8 mi''8 |
        re''2\tr do''4 |
        si'4 si'4 do''4 |
        sold'2 si'4 |
        mi''4 mi''4 mi''4 |
        do''4 do''4 do''4 |
        si'4 si'4.\tr la'8 |
        la'2 r4 |
        R2.*3 |
        r4 r fa''8 mi''8 |
        re''2\tr do''4 |
        si'4 si'4 do''4 |
        sold'2
      }
    >>
    <<
      { \tag #'vdessus \voiceOne si'4 |
        do''4 re''4 mi''4 |
        re''4\tr do''4 si'4 |
        do''4 si'4.\tr la'8 |
        la'2 \oneVoice }
      \tag #'vdessus \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sold'4 |
        la'4 si'4 do''4 |
        si'4 la'4 sold'4 |
        la'4 sold'4. la'8 |
        la'2
      }
    >>
    <<
      \tag #'basse { r4 R2.*4 }
      \tag #'vdessus {
        do''4 |
        la'4 la'4 si'4 |
        sold'4 sold'4 do''4 |
        si'4 si'4.\tr la'8 |
        la'2. |
      }
    >>
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" la'4 la'4 mi'4 |
    la'2 la'4 |
    sol'4 sol'4 sol'8 sol'8 |
    sol'4 re'4 mi'4 |
    fa'2 fa'4 |
    mi'4 mi'4 mi'8 mi'8 |
    fa'2 do'4 |
    re'4 re'4 sol'4 |
    sol'2 sol'4 |
    mi'4 re'4 do'4 |
    si4 do'4 sol'4 |
    fa'4 sol'8[ fa'8] sol'4 |
    do'2 mi'4 |
    la'4 la'4 la'4 |
    fad'4 fad'4 sol'4 |
    sol'4 fad'4. mi'8 |
    mi'2 mi'4 |
    do'4 si4 la4 |
    sold4 la4 mi'4 |
    re'4 mi'8[ re'8] mi'4 |
    la4 la'4 la'8 la'8 |
    fad'2 fad'4 |
    sol'4 sol'4 sol'8 sol'8 |
    sol'4 sol'4 sol'4 |
    la'2 la'4 |
    fad'4 fad'4 mi'8 mi'8 |
    fad'2 sol'4 |
    fad'4 fad'4 mi'4 |
    red'2 r4 |
    R2.*3 |
    r4 r sol'8 sol'8 |
    sol'2 si'4 |
    fad'4 fad'4 sol'4 |
    fad'2 re'4 |
    si4 la4 sol4 |
    fad4 sol4 re'4 |
    do'4 re'8 [ do'8 ] re'4 |
    sol2 sol'4 |
    sol'4 si'4 si'4 |
    la'4 la'4 la'4 |
    la'4 sol'4. sol'8 |
    mi'2\tr sol'8 sol'8 |
    sold'2 la'4 |
    mi'4 mi'4 mi'4 |
    mi'2 sold'4 |
    la'4 la'4 mi'4 |
    fa'4 fa'4 fa'4 |
    fa'4 mi'4. mi'8 |
    dod'2 r4 |
    R2.*3 |
    r4 r la'8 la'8 |
    sold'2 la'4 |
    mi'4 mi'4 mi'4 |
    mi'2 mi'4 |
    do'4 si4 la4 |
    sold4 la4 mi'4 |
    re'4 mi'8 [ re'8 ] mi'4 |
    la2 mi'4 |
    fa'4 fa'4 fa'4 |
    mi'4 mi'4 mi'4 |
    fa'4 mi'4 si4 |
    do'2. |
  }
  \tag #'vtaille {
    \clef "vtaille" 
    do'4 do'4 do'4 |
    do'2 do'4 |
    do'4 do'4 mi'8 mi'8 |
    re'4 si4 sol4 |
    la2 re'4 |
    do'4 do'4 do'8 do'8 |
    do'2 mi'4 |
    si4 si4 do'4 |
    si2 r4 |
    R2.*3 |
    r4 r do'4 |
    do'4 do'4 do'4 |
    si4 si4 si4 |
    si4 si4. si8 |
    sold2 r4 |
    R2.*3 |
    r4 mi'4 mi'8 mi'8 |
    re'2 re'4 |
    re'4 re'4 re'8 re'8 |
    sol4 si4 si4 |
    la2 la4 |
    si4 si4 si8 si8 |
    la2 si4 |
    si4 si4 si4 |
    si2 r4 |
    R2.*3 |
    r4 r mi'8 mi'8 |
    mi'2 sol'4 |
    do'4 do'4 re'4 |
    re'2 r4 |
    R2.*3 |
    r4 r si4 |
    mi'4 mi'4 mi'4 |
    do'4 do'4 do'4 |
    do'4 si4.\tr do'8 |
    do'2 do'8 do'8 |
    re'2 mi'4 |
    re'4 re'4 do'4 |
    si2\tr si4 |
    do'4 do'4 do'4 |
    la4 la4 la4 |
    la4 sold4. la8 |
    la2 r4 |
    R2.*3 |
    r4 r re'8 re'8 |
    re'2 mi'4 |
    re'4 re'4 do'4 |
    si2\tr r4 |
    R2.*3 |
    r4 r do'4 |
    do'4 do'4 re'4 |
    si4 si4 la4 |
    la4 sold4. la8 |
    la2. |
  }
  \tag #'vbasse {
    \clef "vbasse" la4 la4 la4 |
    fa2 fa4 |
    do'4 do'4 do8 do8 |
    sol4 sol4 sol4 |
    re2 re4 |
    la4 la4 la8 sol8 |
    fa2 mi4 |
    re4 re4 do4 |
    sol2 r4 |
    R2.*3 |
    r4 r do'4 |
    la4 la4 la4 |
    si4 si4 sol4 |
    mi4 si,4 si,4 |
    mi2 r4 |
    R2.*3 |
    r4 la4 la8 la8 |
    re'2 re4 |
    sol4 sol4 sol8 sol8 |
    mi4 mi4 mi4 |
    la2 fad4 |
    si4 si4 sol8 sol8 |
    fad2 mi4 |
    red4 red4 mi4 |
    si,2 r4 |
    R2.*3 |
    r4 r mi'8 re'8 |
    do'2 si4 |
    la4 la4 sol4 |
    re2 r4 |
    R2.*3 |
    r4 r sol4 |
    mi4 mi4 mi4 |
    la4 la4 la4 |
    fa4 sol8[ fa8] sol4 |
    do2 do'8 do'8 |
    si2 la4 |
    sold4 sold4 la4 |
    mi2 mi4 |
    do4 do4 do4 |
    fa4 fa4 fa4 |
    re4 mi8[ re8] mi4 |
    la,2 r4 |
    R2.*3 |
    r4 r re'8 do'8 |
    si2 la4 |
    sold4 sold4 la4 |
    mi2 r4 |
    R2.*3 |
    r4 r la4 |
    fa4 fa4 re4 |
    mi4 mi4 do4 |
    re4 mi8[ re8] mi4 |
    la,2. |
  }
>>
