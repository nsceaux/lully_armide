\piecePartSpecs
#`((dessus #:indent 0
           #:tag-notes tous
           #:score-template "score-voix")
   (haute-contre #:indent 0
                 #:score-template "score-voix")
   (taille #:indent 0
           #:score-template "score-voix")
   (quinte #:indent 0
           #:score-template "score-voix")
   (basse #:indent 0
          #:score-template "score-basse-continue-voix"
          #:tag-notes tous))
