\livretAct PROLOGUE
\livretDescAtt\column {
  \wordwrap-center { Le Theatre represente un Palais. }
  \null
  \wordwrap-center {
    La Gloire, la Sagesse, Suite de la Gloire & de la Sagesse.
  }
}
\livretPers La Gloire
\livretRef #'AABgloireSagesse
%# Tout doit ceder dans l'Univers
%# A l'Auguste Heros que j'aime.
%# L'effort des Ennemis, les glaces des Hyvers,
%# Les Rochers, les Fleuves, les Mers,
%# Rien n'arreste l'ardeur de sa valeur extréme.
\livretPers La Sagesse
%# Tout doit ceder dans l'Univers
%# A l'Auguste Heros que j'aime.
%# Il sçait l'art de tenir tous les Monstres aux fers:
%# Il est Maistre absolu de cent Peuples divers,
%# Et plus Maistre encor de luy-mesme.
\livretPers La Gloire & la Sagesse
%# Tout doit ceder dans l'Univers
%# A l'Auguste Heros que j'aime.
\livretPers La Sagesse & sa suite
%# Chantons la douceur de ses Loix.
\livretPers La Gloire & sa suite
%# Chantons ses glori=eux Exploits.
\livretPersDidas La Gloire & la Sagesse ensemble
\livretRef #'AACgloireSagesse
%# D'une esgale tendresse,
%# Nous aimons le mesme Vainqueur.
\livretPers La Sagesse
%#- Fiere Gloire, c'est vous,
\livretPers La Gloire
%#= C'est vous, douce Sagesse,
\livretPers La Gloire & la Sagesse
%# C'est vous, qui partagez avec moy son grand cœur.
\livretPers La Gloire
%# Je l'emportois sur vous tant qu'a duré la guerre;
%# Mais dans la paix vous l'emportez sur moy.
%# Vous reglez en secret avec ce sage Roy
%# Le destin de toute la terre.
\livretPers La Sagesse
%# La Victoire a suivy ce Heros en tous lieux;
%# Mais pour montrer son amour pour la Gloire,
%# Il se sert encor mieux
%# De la paix que de la victoire.
%# Au milieu du repos qu'il asseure aux Humains,
%# Il fait tomber sous ses puissantes mains
%# Un Monstre qu'on a creû si longtemps invincible:
%# On voit dans ses travaux combien il est sensible
%# Pour vostre immortelle Beauté.
%# Il previent vos desirs, il passe vostre attente,
%# L'amour dont il vous aime incessemment s'augmente,
%# Et n'a jamais tant esclaté.
%# Qu'un vain desir de preference,
%# N'altere point l'intelligence
%# Que ce Heros entre nous veut former:
%# Disputons seulement à qui sçait mieux l'aimer.
\livretDidasP\justify { La Gloire repete ce dernier Vers avec la Sagesse. }
\livretPersDidas La Gloire & la Sagesse ensemble
%# Dés qu'on le voit paraistre,
%# De quel cœur n'est-il point le Maistre?
%# Qu'il est doux de suivre ses pas!
%# Peut-on le connaistre,
%# Et ne l'aimer pas?
\livretRef #'AADentree
\livretDidasPPage\justify {
  Les Chœurs repetent ces cinq derniers vers. Et la Suite de la gloire
  & celle de la Sagesse témoignent par des Danses la joye qu'elles ont
  de voir ces deux Divinitez dans une intelligence parfaite.
}
\livretPers La Sagesse
\livretRef #'AAGgloireSagesse
%# Suivons nostre Heros, que rien ne nous separe.
%# Il nous invite aux Jeux qu'on luy prepare:
%# Nous y verrons Renaud malgré la Volupté,
%# Suivre un Conseil fidele & sage,
%# Nous le verrons sortir du Palais enchanté,
%# Où par l'amour d'Armide il estoit arresté,
%# Et voler où la gloire appelle son courage,
%# Le grand Roy qui partage entre nous ses desirs,
%# Aime à nous voir mesme dans ses Plaisirs.
\livretPers La Gloire
%# Que l'esclat de son nom s'estende au bout du Monde,
%# Ré=ünissons nos voix, Que chacun nous réponde.
\livretPers La Gloire & la Sagesse & les chœurs
%# Chantons la douceur de ses Loix,
%# Chantons ses glori=eux Exploits.
\livretRef #'AAHentree
\livretDidasPPage\justify {
  La suite de la Gloire & celle de la Sagesse continüent leur
  réjoüissance.
}
\livretPers Les chœurs
\livretRef #'AAKchoeur
%# Que dans le Temple de Memoire
%# Son nom soit pour jamais gravé;
%# C'est à luy qu'il est reservé,
%# D'unir la Sagesse & la Gloire.
\livretFinAct Fin du Prologue
\sep
