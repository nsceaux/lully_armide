\livretAct ACTE IV
\livretScene SCENE I
\livretDescAtt\wordwrap-center { UBALDE, & le Chevalier Danois. }
\livretDidasP\justify {
  Ubalde porte un bouclier de Diamans, & tient un Sceptre d’or,
  qui luy ont été donné par un Magicien, pour dissiper les enchantemens
  d’Armide, & pour délivrer Renaud.
  Le Chavalier Danois porte une Epée qu’il doit presenter à Renaud.
}
\livretDidasP\justify {
  Une vapeur s’éleve & se répand dans le Desert qui a paru au
  troisiéme Acte.
  Des antres & des abismes s’ouvrent, & il en sort des bestes farouches
  & des Monstres épouventables.
}
\livretPersDidas Ubalde, & le Chevalier Danois ensemble
\livretRef #'EAAubaldeChevalier
%# Nous ne trouvons par tout que des Gouffres ouverts,
%# Armide a dans ces lieux transporté les Enfers.
%# Ah! que d'objets horribles!
%# Que de Monstres terribles!
\livretDidasP\justify {
  Le Chevalier Danois attaque les Monstres, Ubalde le retient,
  & lui montre le Sceptre d'or qu'il porte, & qui leur a été
  donné pour dissiper les Enchantemens.
}
\livretPers Ubalde
%# Celuy qui nous envoye =a preveu ce danger,
%# Et nous a monstré l'art de nous en dégager.
%# Ne craignons point Armide ny ses charmes:
%# Par ce secours plus puissant que nos armes,
%# Nous en serons aisément garentis.
%# Laissez-nous un libre passage,
%# Monstres, allez cacher vostre inutile rage
%# Dans les gouffres profonds dont vous estes sortis.
\livretDidasP\justify {
  Les Monstres s’abisment, la vapeur se dissipe, le Desert disparoit,
  & se change en une Campagne agreable, bordée d’arbres chargez de
  fruits & arrosée de Ruisseaux.
}
\livretPers Le Chevalier Danois
%# Allons chercher Renaud, le Ciel nous favorise
%# Dans nostre penible enreprise.
%# Ce qui peut flater nos desirs
%# Doit à son tour tenter de nous surprendre.
%# C'est desormais du charme des plaisirs
%# Que nous aurons à nous deffendre.
\livretPersDidas Ubalde & le Chevalier Danois ensemble
%# Redoublons nos soins, gardons-nous
%# Des perils agre=ables.
%# Les enchantements les plus doux
%# Sont les plus redoutables.
\livretPers Ubalde
%# On voit d'icy le sejour enchanté
%# D'Armide & du Heros qu'elle aime,
%# Dans ce Palais Renaud est arresté
%# Par un charme fatal dont la force est extréme.
%# C'est la que ce Vainqueur si fier, si redouté,
%# Oubli=ant tout jusqu'à luy mesme,
%# Est reduit à languir avec indignité
%# Dans une molle oisiveté.
\livretPers Le Chevalier Danois
%# En vain tout l'Enfer s'interesse
%# Dans l'Amour qui seduit un cœur si glori=eux:
%# Si sur ce Boucli=er Renaud tourne les yeux
%# Il rougira de sa foiblesse,
%# Et nous l'engagerons à partir de ces lieux.

\livretScene SCENE II
\livretDescAtt\center-column {
  \justify {
    Un Demon sous la figure de Lucinde, fille Danoise, aimée du
    Chevalier Danois. Troupe de Demons transformez en habitans
    Champestres de l’Isle qu’Armide a choisie pour y retenir Renaud
    enchanté.
  }
  \line { UBALDE, le Chavelier Danois. }
}
\livretPers Lucinde
\livretRef #'EBAlucinde
%# Voicy la charmante Retraite
%# De la felicité parfaite:
%# Voicy l'*heureux sejour
%# Des Jeux & de l'Amour.
\livretPers Le chœur
%# Voicy la charmante Retraite
%# De la felicité parfaite:
%# Voicy l'*heureux sejour
%# Des Jeux & de l'Amour.
\null
\livretRef #'EBBgavotte
\livretDidasPPage\justify {
  Les Habitans Champestres dansent.
}
\livretPersDidas Ubalde parlant au Chevalier Danois
\livretRef #'EBDlucindeChevalierUbalde
%# Allons, qui vous retient encore?
%# Allons, c'est trop vous arrester.
\livretPers Le Chevalier Danois
%# Je voy la Beauté que j'adore:
%# C'est elle, je n'en puis douter.
\livretPers Lucinde & le chœur
%# Jamais dans ces beaux lieux nostre attente n'est vaine,
%# Le bien que nous cherchons se vient offrir à nous,
%# Et pour l'avoir trouvé sans peine
%# Nous ne l'en trouvons pas moins doux.
\livretPers Le chœur
%# Voicy la charmante Retraite
%# De la felicité parfaite;
%# Voicy l'*heureux sejour
%# Des Jeux & de l'Amour.
\livretPersDidas Lucinde parlant au Chevalier Danois
%# Enfin je voy l'Amant pour qui mon cœur soûpire,
%# Je retrouve le bien que j'ay tant souhaité.
\livretPers Le Chevalier Danois
%# Puis-je voir icy la Beauté
%# Qui m'a soûmis à son Empire?
\livretPers Ubalde
%# Non, ce n'est qu'un charme trompeur
%# Dont il faut garder vostre cœur.
\livretPers Le Chevalier Danois
%# Si loin des bords glacez où vous pristes naissance,
%# Qui peut vous offrir à mes yeux?
\livretPers Lucinde
%# Par une magique puissance
%# Armide m'a conduite en ces aimables lieux,
%# Et je vivois dans la douce esperance
%# D'y voir bien-tost ce que j'aime le mieux.
%# Goustons les doux plaisirs que pour nos cœurs fidelles
%# Dans cét heureux sejour l'Amour a preparez.
%# Le Devoir par des loix cru=elles
%# Ne nous a que trop separez.
\livretPers Ubalde
%# Fuy=ez, faites-vous vi=olence.
\livretPers Le Chevalier Danois
%# L'Amour ne me le permet pas.
%# Contre de si charmants appas
%# Mon cœur est sans deffense.
\livretPers Ubalde
%# Est-ce la cette fermeté
%# Dont vous vous estes tant vanté?
\livretPersDidas Le Chevalier Danois & Lucinde ensemble
%# Joü=issons d'un bonheur extréme,
%# Hé! quel autre bien peut valoir
%# Le plaisir de voir ce qu'on aime?
%# Hé! quel autre bien peut valoir
%# Le plaisir de vous voir?
\livretPers Ubalde
%# Malgré la puissance infernale,
%# Malgré vous mesme il faut vous détromper.
%# Ce Sceptre d'or peut dissiper
%# Une erreur si fatale.
\livretDidasP\justify {
  Ubalde touche Luncinde avec le Sceptre d’or qu’il tient,
  & Lucinde disparoit aussi tost.
}

\livretScene SCENE III
\livretDescAtt\wordwrap-center { LE CHEVALIER DANOIS, UBALDE. }
\livretPers Le Chevalier Danois
\livretRef #'ECAchevalierUbalde
%# Je tourne en vain les yeux de toutes parts,
%# Je ne voy plus cette Beauté si chere.
%# Elle eschape à mes regards
%# Comme une vapeur legere.
\livretPers Ubalde
%# Ce que l'Amour a de charmant
%# N'est qu'une illusi=on qui ne laisse apres elle
%# Qu'une honte éternelle.
%# Ce que l'Amour a de charmant
%# N'est qu'un funeste enchantement.
\livretPers Le Chevalier Danois
%# Je voy le danger où s'expose
%# Un cœur qui ne fuit pas un charme si puissant.
%# Que vous estes heureux si vous estes exempt
%# Des foiblesses que l'amour cause!
\livretPers Ubalde
%# Non, je n'ay point gardé mon cœur jusqu'à ce jour,
%# Prés de l'objet que j'aime il m'estoit doux de vivre.
%# Mais quand la Gloire ordonne de la suivre
%# Il faut laisser gemir l'Amour.
%# Des charmes les plus forts la raison me desgage.
%# Rien ne nous doit icy retenir davantage,
%# Profitons des conseils que l'on nous a donnez.

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  Un demon sous la figure de Melisse fille Italienne aimée d'Ubalde,
  le Chevalier Danois, Ubalde.
}
\livretPers Melisse
\livretRef #'EDAmelisseChevalierUbalde
%# D'où vient que vous vous destournez
%# De ces eaux & de cét ombrage?
%# Goustez un doux repos, Estrangers fortunez,
%# Delassez-vous icy d'un penible voy=age.
%# Un favorable sort vous appelle au partage
%# Des biens qui vous sont destinez.
\livretPers Ubalde
%# Est-ce vous, charmante Melisse?
\livretPers Melisse
%# Est-ce vous, cher Amant, Est-ce vous que je voy?
\livretPersDidas Ubalde, & Melisse ensemble
%# Au rapport de mes yeux je n'ose adjoûter foy.
%# Se peut-il qu'en ces lieux l'Amour nous ré=ünisse?
\livretPers Melisse
%# Est-ce vous, cher Amant, est-ce vous que je voy?
\livretPers Ubalde
%# Est-ce vous, charmante Melisse?
\livretPers Le Chevalier Danois
%# Non, ce n'est qu'un charme trompeur,
%# Dont il faut garder vostre cœur.
%# Fuy=ez, faites vous vi=olence.
\livretPers Melisse
%# Pourquoy faut-il encor m'arracher mon Amant?
%# Faut-il ne nous voir qu'un moment
%# Apres une si longue absence?
%# Je ne puis consentir à vostre esloignement.
%# Je n'ay que trop souffert un si cru=el tourment,
%# & je mourray s'il recommence.
\livretPersDidas Ubalde & Melisse ensemble
%# Faut-il ne nous voir qu'un moment
%# Aprés une si longue absence?
\livretPers Le Chevalier Danois
%# Est-ce là cette fermeté
%# Dont vous vous estes tant vanté?
%# Sortez de vostre erreur, la Raison vous appelle.
\livretPers Ubalde
%# Ah! que le Raison est cru=elle!
%# Si je suis abusé, pourquoy m'en avertir?
%# Que mon erreur me paroist belle!
%# Que je serois heureux de n'en jamais sortir!
\livretPers Le Chevalier Danois
%# J'auray soin malgré vous de vous en garentir.
\livretDidasP\justify {
  Le Chevalier Danois oste le Sceptre d’or des mains d’Ubalde,
  il en touche Melisse, & la fait disparoistre.
}
\livretPers Ubalde
%# Que devient l'objet qui m'enflame?
%# Melisse disparoist soudain!
%# Ciel! faut-il qu'un fantosme vain
%# Cause tant de trouble à mon ame?
\livretPers Le Chevalier Danois
%# Ce que l'amour a de charmant
%# N'est qu'une illusi=on qui ne laisse apres elle
%# Qu'une honte éternelle.
%# Ce que l'Amour a de charmant
%# N'est qu'un funeste enchantement.
\livretPers Ubalde & Le Chevalier Danois
%# Ce que l'Amour a de charmant
%# N'est qu'un funeste enchantement.
\livretPers Ubalde
%# D'une nouvelle erreur songeons à nous deffendre,
%# Evitons de trompeurs attraits,
%# Ne nous destournons pas du chemin qu'il faut prendre
%# Pour arriver à ce Palais.
\livretPers Ubalde & Le Chevalier Danois
%# Fuy=ons les douceurs dangeureuses
%# Des illusi=ons amoureuses:
%# On s'esgare quand on les suit;
%# Heureux qui n'en est pas seduit!
\livretFinAct Fin du quatriéme Acte
\sep
